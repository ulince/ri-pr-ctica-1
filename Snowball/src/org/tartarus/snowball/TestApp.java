package org.tartarus.snowball;

import java.lang.reflect.Method;
import java.util.*;
import java.io.Reader;
import java.io.Writer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.sql.*;

public class TestApp {

	private static void usage() {
		System.err
				.println("Usage: TestApp <algorithm> <input file> [-o <output file>]");
	}

	public static void main(String[] args) throws Throwable {
		// clase de conexi�n a la BD
		DBConnection db;
		// ResultSet para guardar lo recuperado de la DB
		ResultSet rs = null;
		// nombre del documento
		String file;
		// lista donde se almacenar�n las stopwords
		List<String> stopwords;
		Reader reader;

		if (args.length < 2) {
			usage();
			return;
		}

		stopwords = new ArrayList<String>();

		Class stemClass = Class.forName("org.tartarus.snowball.ext." + args[0]
				+ "Stemmer");
		SnowballStemmer stemmer = (SnowballStemmer) stemClass.newInstance();

		reader = new InputStreamReader(new FileInputStream(args[1]));
		file = fileName(args[1]);
		reader = new BufferedReader(reader);
		StringBuffer input = new StringBuffer();
		OutputStream outstream;
		/* Clase de conexi�n a la base de datos */
		db = new DBConnection();

		/* Obtener la lista de stopwords de la base de datos */
		String str = "Select * from stopwords";
		try {
			stopwords = db.readDataBase(str);
		} catch (SQLException e) {
			System.out.println("Error recuperando stopwords");
		}

		/*----------------------*/

		if (args.length > 2) {
			if (args.length >= 4 && args[2].equals("-o")) {
				outstream = new FileOutputStream(args[3]);
			} else {
				usage();
				return;
			}
		} else {
			outstream = System.out;
		}
		Writer output = new OutputStreamWriter(outstream);
		output = new BufferedWriter(output);

		int repeat = 1;
		if (args.length > 4) {
			repeat = Integer.parseInt(args[4]);
		}

		Object[] emptyArgs = new Object[0];
		int character;
		while ((character = reader.read()) != -1) {
			char ch = (char) character;
			// si ch es un gui�n, se cambia por un espacio
			if (ch == '-') {
				ch = ' ';
			}
			if (Character.isWhitespace((char) ch)) {
				if (input.length() > 0) {
					/* Checar si la palbra a lematizar es una stopword */
					if (stopwords.contains(input.toString())) {
						input.delete(0, input.length());
						continue;
					}
					/*-------------------*/

					stemmer.setCurrent(input.toString());
					for (int i = repeat; i != 0; i--) {
						stemmer.stem();
					}

					String curr = stemmer.getCurrent();
					System.out.println(curr);

					/*
					 * Insertar la palabra a la tabla de t�rminos y en la tabla
					 * de frecuencias para cada documento con una frecuencia
					 * inicial de 0
					 */
					String query = "insert into frecuencia (uri, ra�z) values (?,?)";
					try {
						db.Insert(curr);
						for (int i = 1; i < 11; i++) {
							db.Insert(query, "d" + i, curr);
						}
					} catch (SQLException e) {
						// la palabra ya existe en los t�rminos, no hacer nada
					}
					/*---------*/

					/* Cambiar la frecuencia del t�rmino en el documento */
					query = "insert into frecuencia (uri, ra�z) values (?,?) on duplicate key update frecuencia = frecuencia + 1";
					try {
						db.Insert(query, file, curr);
					} catch (SQLException e) {
						//no hacer nada

					}

					/*---------*/

					output.write(curr);
					output.write('\n');
					input.delete(0, input.length());
				}
			} else {
				// checar si el caracter es una letra o no
				if (!Character.isLetter((char) ch)) {
					if (ch != '\'' && ch != '-') {
						continue;
					}
				}
				input.append(Character.toLowerCase(ch));
			}
		}
		
		/*Se lleg� al final del documento, procesar la �ltima palabra*/
		
		if (input.length() > 0) {
			/* Checar si la palbra a lematizar es una stopword */
			if (stopwords.contains(input.toString())) {
				input.delete(0, input.length());
				return;
			}
			/*-------------------*/

			stemmer.setCurrent(input.toString());
			for (int i = repeat; i != 0; i--) {
				stemmer.stem();
			}

			String curr = stemmer.getCurrent();
			System.out.println(curr);

			/*
			 * Insertar la palabra a la tabla de t�rminos y en la tabla
			 * de frecuencias para cada documento con una frecuencia
			 * inicial de 0
			 */
			String query = "insert into frecuencia (uri, ra�z) values (?,?)";
			try {
				db.Insert(curr);
				for (int i = 1; i < 11; i++) {
					db.Insert(query, "d" + i, curr);
				}
			} catch (SQLException e) {
				// la palabra ya existe en los t�rminos, no hacer nada
			}
			/*---------*/

			/* Cambiar la frecuencia del t�rmino en el documento */
			query = "insert into frecuencia (uri, ra�z) values (?,?) on duplicate key update frecuencia = frecuencia + 1";
			try {
				db.Insert(query, file, curr);
			} catch (SQLException e) {
				//no hacer nada

			}

			/*---------*/

			output.write(curr);
			output.write('\n');
			input.delete(0, input.length());
		
		/*-----------*/		
		}
		output.flush();
	}

	public static String fileName(String fullPath) {
		int dot = fullPath.lastIndexOf('.');
		int sep = fullPath.lastIndexOf('\\');
		return fullPath.substring(sep + 1, dot);
	}

}
