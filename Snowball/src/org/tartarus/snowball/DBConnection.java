package org.tartarus.snowball;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {

	private String myUrl;
	private Connection conn;

	public DBConnection() {
		myUrl = "jdbc:mysql://localhost:3306/documentos";

	}

	public void Insert(String str) throws SQLException {
		try {
			conn = DriverManager.getConnection(myUrl, "root", "admin");
			// the mysql insert statement
			String query = " insert into termino (ra�z)" + " values (?)";

			// create the mysql insert preparedstatement
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, str);
			// execute the preparedstatement
			preparedStmt.execute();

			conn.close();
		} catch (Exception e) {
			//System.err.println("Got an exception!");
			//System.err.println(e.getMessage());
		} finally {
			conn.close();
		}
	}
	
	public void Insert(String query, String str1, String str2) throws SQLException {
		try {
			conn = DriverManager.getConnection(myUrl, "root", "admin");
			// the mysql insert statement
			//String query = "insert into frecuencia (uri, ra�z)" + " values (?,?)";

			// create the mysql insert preparedstatement
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, str1);
			preparedStmt.setString(2, str2);
			// execute the preparedstatement
			preparedStmt.execute();

		
		} catch (Exception e) {
			//System.err.println("Got an exception!");
			//System.err.println(e.getMessage());
		} finally {
			conn.close();
		}
	}

	public List<String> readDataBase(String str) throws SQLException {

		List<String> list = new ArrayList<String>();
		try {
			conn = DriverManager.getConnection(myUrl, "root",
					"admin");
			// the mysql select statement
			String query = str;

			// Statements allow to issue SQL queries to the database
			Statement statement = conn.createStatement();
			// Result set get the result of the SQL query
			ResultSet rs = statement.executeQuery(query);
			// writeResultSet(resultSet);	
			
			//Tomar las stopwords del ResultSet y ponerlos en una lista
		    while(rs.next()){
		    	String temp = rs.getString("words");
		    	list.add(temp);
		    }    
		    return list;
		} catch (Exception e) {
			//System.err.println("Got an exception!");
			//System.err.println(e.getMessage());		
			return list;
		} finally {
			conn.close();
		}

	}
}
